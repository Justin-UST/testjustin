FROM node:14.9.0

RUN npm install -g apiconnect
RUN npm install -g newman
RUN npm install -g newman-reporter-json-summary